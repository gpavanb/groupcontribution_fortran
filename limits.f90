module limits
  ! Spreadsheet limits
  integer, parameter::NG = 121
  integer, parameter::NC = 10
  real(KIND=8), parameter::CUBIC_THRESHOLD = 0.0001
end module limits
