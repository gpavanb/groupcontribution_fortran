module readFuel
  use limits
  use precision
  implicit none

  ! Header contains number of rows and columns
  ! Columns contain only functional groups of current fuel
  ! Names not supported because utter pain to convert 

  type fuelMat_type
    integer::numRows
    integer::numColumns
    integer, dimension(:), allocatable::indexList
    integer, dimension(:,:), allocatable::groupCounts
	real(WP), dimension(:), allocatable::x_init
  end type fuelMat_type

  type(fuelMat_type)::fuelMat
  save

contains
  ! use parser and replace read statements
  subroutine readFuelTable(fuelName)
    integer::allocateStatus
    integer::IOStatus
 
    ! Loop variables
    integer::i,j

    character(LEN=*), intent(in)::fuelName

    ! Look at folder for list of compatible fuel names
    open(1,file="data/compound_description/"//trim(fuelName) &
         //".txt",status='old',action='read',iostat=IOStatus)

    if (IOStatus > 0) then
       print *, "ERROR: Unable to open fuel description file"
       stop
    end if

    ! Read fuel description
    read(1,*) fuelMat%numRows, fuelMat%numColumns

    allocate(fuelMat%indexList(fuelMat%numColumns), STAT = allocateStatus)
    allocate(fuelMat%groupCounts(fuelMat%numRows, &
             fuelMat%numColumns), STAT=allocateStatus)

    if (AllocateStatus /= 0) then 
      print *, "ERROR : Unable to allocate memory for fuel description matrix"
      stop
    end if 

    read(1,*) (fuelMat%indexList(j),j=1,fuelMat%numColumns)
    read(1,*) ((fuelMat%groupCounts(i,j), j=1,fuelMat%numColumns) &
                ,i=1,fuelMat%numRows)

    close (1)
  end subroutine readFuelTable
  
  subroutine readInitFuel(fuelName)
      integer::allocateStatus
      integer::IOStatus
 
      ! Loop variables
      integer::i
	  
	  character(LEN=*), intent(in)::fuelName

      ! Look at folder for list of compatible fuel names
      open(1,file="data/init_data/"//trim(fuelName) &
           //".txt",status='old',action='read',iostat=IOStatus)

      if (IOStatus > 0) then
         print *, "ERROR: Unable to open fuel initial composition file"
         stop
      end if

      allocate(fuelMat%x_init(fuelMat%numRows), STAT = allocateStatus)

      if (AllocateStatus /= 0) then 
        print *, "ERROR : Unable to allocate memory for initial fuel composition"
        stop
      end if 

      read(1,*) (fuelMat%x_init(i),i=1,fuelMat%numRows)

      close (1)
  end subroutine readInitFuel

  subroutine readFuel_deallocate
    deallocate(fuelMat%indexList)
    deallocate(fuelMat%groupCounts)
  end subroutine readFuel_deallocate

end module readFuel
