module fuelProp
  use compound_properties
  use precision
  use readFuel, only : fuelMat
  implicit none

  type fuelProp_type
    real(WP), allocatable::MW(:)
    real(WP), allocatable::Vc(:)
    real(WP), allocatable::Tc(:)
    real(WP), allocatable::Pc(:)
    real(WP), allocatable::Hv(:)
    real(WP), allocatable::CpA(:)
    real(WP), allocatable::CpB(:)
    real(WP), allocatable::CpC(:)
    real(WP), allocatable::omega(:)
    real(WP), allocatable::specVol(:)
    real(WP), allocatable::eps(:)
    real(WP), allocatable::sigma(:)
    real(WP), allocatable::a(:)
    real(WP), allocatable::b(:)
    real(WP), allocatable::kappa(:)
    real(WP), allocatable::Zc(:)
	
	! Activity coefficient
	real(WP), allocatable::Q(:)
	real(WP), allocatable::R(:)
	real(WP), allocatable::capQ(:)
	real(WP), allocatable::capR(:)
	real(WP), dimension(:,:),allocatable::aMat
  end type fuelProp_type

  type(fuelProp_type) fuel
  save

  private::allocateFuel
  integer, private::allocateStatus

contains
  subroutine allocateFuel
    allocate(fuel%MW(fuelMat%numRows), STAT = allocateStatus)
    allocate(fuel%Vc(fuelMat%numRows), STAT = allocateStatus)
    allocate(fuel%Tc(fuelMat%numRows), STAT = allocateStatus)
    allocate(fuel%Pc(fuelMat%numRows), STAT = allocateStatus)
    allocate(fuel%Hv(fuelMat%numRows), STAT = allocateStatus)
    allocate(fuel%CpA(fuelMat%numRows), STAT = allocateStatus)
    allocate(fuel%CpB(fuelMat%numRows), STAT = allocateStatus)
    allocate(fuel%CpC(fuelMat%numRows), STAT = allocateStatus)
    allocate(fuel%omega(fuelMat%numRows), STAT = allocateStatus)
    allocate(fuel%specVol(fuelMat%numRows), STAT = allocateStatus)
    allocate(fuel%eps(fuelMat%numRows), STAT = allocateStatus)
    allocate(fuel%sigma(fuelMat%numRows), STAT = allocateStatus)
    allocate(fuel%a(fuelMat%numRows), STAT = allocateStatus)
    allocate(fuel%b(fuelMat%numRows), STAT = allocateStatus)
    allocate(fuel%kappa(fuelMat%numRows), STAT = allocateStatus)
    allocate(fuel%Zc(fuelMat%numRows), STAT = allocateStatus)
	
	! Activity coefficient
	allocate(fuel%Q(fuelMat%numRows), STAT = allocateStatus)
	allocate(fuel%R(fuelMat%numRows), STAT = allocateStatus)
	allocate(fuel%capQ(fuelMat%numColumns), STAT = allocateStatus)
	allocate(fuel%capR(fuelMat%numColumns), STAT = allocateStatus)
	allocate(fuel%aMat(fuelMat%numColumns, fuelMat%numColumns), STAT = allocateStatus)
  end subroutine allocateFuel

  subroutine setFuelProp()
    call allocateFuel

    if (allocateStatus > 0) then
      print *, "ERROR: Unable to allocate fuel properties"
    end if 

    fuel%MW = MWVec()
    fuel%Vc = VcVec()
    fuel%Tc = TcVec()
    fuel%Pc = PcVec()
    fuel%Hv = HvVec()
    fuel%CpA = CpA()
    fuel%CpB = CpB()
    fuel%CpC = CpC()
    fuel%omega = omegaVec()
    fuel%specVol = specVol_lVec()
    fuel%eps = epsVec()
    fuel%sigma = sigmaVec()
    fuel%a = aVec()
    fuel%b = bVec()
    fuel%kappa = kappaVec()
    fuel%Zc = ZcVec()
	
	! Activity Coefficient
	fuel%Q = QVec()
	fuel%R = RVec()
	fuel%capQ = capQVec()
	fuel%capR = capRVec()
	fuel%aMat = aMat()
  end subroutine setFuelProp

  subroutine fuelProp_deallocate
    deallocate(fuel%MW)
    deallocate(fuel%Vc)
    deallocate(fuel%Pc)
    deallocate(fuel%Tc)
    deallocate(fuel%Hv)
    deallocate(fuel%CpA)
    deallocate(fuel%CpB)
    deallocate(fuel%CpC)
    deallocate(fuel%omega)
    deallocate(fuel%specVol)
    deallocate(fuel%eps)
    deallocate(fuel%sigma)
    deallocate(fuel%a)
    deallocate(fuel%b)
    deallocate(fuel%kappa)
    deallocate(fuel%Zc)
	
	! Activity coefficient
	deallocate(fuel%Q)
	deallocate(fuel%R)
	deallocate(fuel%capQ)
	deallocate(fuel%capR)
	deallocate(fuel%aMat)
  end subroutine fuelProp_deallocate
end module fuelProp

    
  
