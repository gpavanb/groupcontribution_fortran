# GroupContribution #

This FORTRAN module is based on the [paper](http://onlinelibrary.wiley.com/doi/10.1002/aic.690401011/abstract) by Constantinou et al. (1994) and calculates physical properties using the group contribution method.

## Summary of set up ##

The code can be compiled by just running. Make sure to have a compatible gfortran compiler.
```
$make
```

This compiles the code in the form of a static library which can then be used in your solver.

In order to test the code, install [fUnit](http://fortranwiki.org/fortran/show/FUnit). You need to set the current FORTRAN compiler before running the tests. In Bash, this can be done using
```
$export FC='gfortran'
```
You can then run the tests by using

```
$make test
```

The GroupContribution module can now be used in your FORTRAN code. A sample program which shows the essential features is given below


```
#!fortran

program simple
  ! Include the module, initialize it, conquer the world and then cleanup                                                     

  ! Module include                
  use groupContribution
  implicit none

  ! Define Variables          
  real(KIND=8), parameter::ambientTemp=300.
  real(KIND=8), dimension(:), allocatable::criticalPressure
  real(KIND=8), dimension(:), allocatable::liquidDensity

  ! Initialize module           
  call init_gc("posf10325")

  ! Perform functions              
  criticalPressure = Pc()
  liquidDensity = rho_l(ambientTemp)

  ! Cleanup         
  call clean_gc
end program simple
```

## Methods ##

All methods return a 1D array containing pure component properties of the mixture. A list of methods has been provided below

** Independent Properties **

* `MW()` : Molecular Weight (in Kg/mol)
* `Pc()` : Critical Pressure (in Pa)
* `Vc()` : Critical Volume (in m <sup>3</sup>)
* `Tc()` : Critical Temperature (in K)
* `omega()` : Acentric Factor
* `specVol_l()` : Molar Liquid Volume at STP (in m <sup>3</sup>)
* `eps()` : Lennard-Jones Energy (in multiples of k)
* `sigma()` : Lennard-Jones Radius (in Å)

** Dependent Properties **

* `c_l(T)` : Liquid specific heat capacity (in J/mol/K)
* `Hv(T)` : Latent heat of vaporization (in J/mol)
* `mu_g(p,T)` : Dynamic Viscosity of vapor (in SI units)
* `mu_l(p,T)` :Dynamic Viscosity of liquid (in SI units)
* `PSat(T)` : Saturated Vapor Pressure (in Pa)
* `rho_l(T)` : Liquid Density(Kg/m <sup>3</sup>) from the Rackett equation
* `rho_l_PR(p,T)` : Liquid Density(Kg/m <sup>3</sup>) from Peng-Robinson
* `rho_g_PR(p,T)` : Gas Density(Kg/m<sup>3</sup>) from Peng-Robinson
* `D(p,T)` : Low pressure diffusion coefficient into air (m <sup>2</sup>/s) and pressure in Pa
* `surf(T)` : Liquid surface tension (in N/m)
* `activity(x_l,T)` : Activity coefficients for given initial mole fractions `x_l`
* `activity(T)` : Activity coefficients based on initial fuel composition

## Tests ##

All tests have been written using [fUnit](http://fortranwiki.org/fortran/show/FUnit)

## Fuels ##

Currently, the code supports the following fuels and it is very easy to add new fuel descriptions to it

* Heptane 
* POSF 4658
* POSF 10264 
* POSF 10289 
* POSF 10325 
* POSF 11498 
* POSF 12341 (High Viscosity)
* POSF 12344 (Low Cetane/Broad Boil)
* POSF 12345 (Flat Boil)

## License ##

Please refer to the LICENSE.pdf in the repository. Note that this code requires PRIOR PERMISSION FROM AUTHORS FOR COMMERCIAL PURPOSES.


## Who do I talk to? ##

* Repo owner or admin : [Pavan Bharadwaj](https://bitbucket.org/gpavanb)
* Other community or team contact : The code was developed at the Flow Physics and Computational Engineering group at Stanford University. Please direct any official queries to [Prof. Matthias Ihme](mailto:mihme@stanford.edu)
