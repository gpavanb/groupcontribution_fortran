module misc
	use precision
	implicit none
	
contains
	
 	subroutine setnan(a,size,val)
	 	real(WP), intent(in)::val
	 	real(WP), dimension(size)::a
		integer, intent(in)::size
		integer::i
	 
		 do i = 1,size
		 	if ( isnan(a(i)) ) then
			 	a(i) = val
		 	end if
	 	end do
 	end subroutine setnan
	
end module misc 