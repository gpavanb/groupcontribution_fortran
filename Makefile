F90 = mpif90
OBJDIR=obj
MODDIR=mod
TESTDIR=tests
LIBDIR=lib

all: libgc.a

OBJ= limits.o \
	precision.o \
	misc.o \
	readProp.o \
	readFuel.o \
	compound_properties.o \
	fuelProp.o \
	pressure_corr.o \
	cubic.o \
	indep_properties.o \
	dep_properties.o \
	groupContribution.o

MOD = $(OBJ:.o=.mod)
OBJ_DEST=$(addprefix $(OBJDIR)/,$(OBJ))
MOD_DEST=$(addprefix $(MODDIR)/,$(MOD))

clean: 
	@funit --clean
	@-rm -f *~
	@-rm -f *.fun
	@-rm -f $(TESTDIR)/*~
	@-rm -f *.o *.mod
	@-rm -f $(OBJDIR)/*.o $(MODDIR)/*.mod
	@-rm -f $(LIBDIR)/libgc.a

test:
	@cp $(TESTDIR)/* .
	@funit cubic
	@funit --clean
	@funit dep_properties
	@funit --clean
	@funit fuelProp
	@funit --clean
	@-rm *.fun  

%.o: %.f90
	@echo "Compiling $*.f90 ..."
	@$(F90) -I$(MODDIR)/ -c $*.f90 -o $(OBJDIR)/$*.o 
	@mv *.mod $(MODDIR)/

libgc.a : $(OBJ)
	@$(AR) rcs $(LIBDIR)/$@ $(OBJ_DEST)
	@chmod 755 $(LIBDIR)/$@
	@ranlib $(LIBDIR)/$@
