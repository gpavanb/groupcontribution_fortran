module groupContribution
  use fuelProp
  use readFuel
  use readProp
  
  ! Modules of functions which can be called
  use dep_properties
  use indep_properties

  implicit none
contains
  subroutine init_gc(fuel)
    character(LEN=*), intent(in)::fuel
    
    call readPropTable
    call readActTable
    call readFuelTable(trim(fuel))
	call readInitFuel(trim(fuel))
    call setFuelProp
  end subroutine init_gc

  subroutine clean_gc
    call readFuel_deallocate
    call fuelProp_deallocate
  end subroutine clean_gc

end module groupContribution
