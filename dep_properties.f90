! TODO : Use only submodules
module dep_properties
  use cubic
  use fuelProp, only : fuel
  use misc
  use precision
  use pressure_corr
  use readFuel, only : fuelMat
  use readProp, only : propMat

  private::omegaD, f0, f1
  
  interface activity
	  module procedure activityTwoArgs, activityoneArg
  end interface

contains

  function rho_l(T)
    real(WP), intent(in)::T
    real(WP), dimension(fuelMat%numRows)::rho_l

    real(WP), dimension(fuelMat%numRows)::phiVec
    real(WP), dimension(fuelMat%numRows)::specVol
    integer::i

    ! RACKETT EQUATION
    do i = 1,fuelMat%numRows
      if (T > fuel%Tc(i)) then
         phiVec(i) = -(1.0_WP - (298/fuel%Tc(i)))**(2.0_WP/7.0_WP)
      else 
         phiVec(i) = (1.0_WP - (T/fuel%Tc(i)))**(2.0_WP/7.0_WP) - &
                     (1.0_WP - (298/fuel%Tc(i)))**(2.0_WP/7.0_WP)
      end if
   end do
      
   specVol = (fuel%specVol)*(fuel%Zc**phiVec)
   rho_l = (fuel%MW)/specVol
 end function rho_l

 function rho_g_PR(p,T)
   real(WP), intent(in)::p
   real(WP), intent(in)::T
   real(WP), dimension(fuelMat%numRows)::rho_g_PR

   real(WP), dimension(fuelMat%numRows)::alpha

   real(WP)::A
   real(WP)::B
   real(WP)::coeff(4)
   real(WP)::root(3)

   integer::i

   alpha = (1.0_WP + (fuel%kappa)*(1.0_WP - ((T/fuel%Tc)**0.5_WP) ))**2.0_WP

   do i = 1, fuelMat%numRows
      A = fuel%a(i)*alpha(i)*p/((8.314_WP**2.0_WP)*(T**2.0_WP))
      B = fuel%b(i)*p/((8.314_WP)*T)

      ! Set coefficients for Peng-Robinson
      coeff(4) = -(A*B - B**2.0_WP - B**3.0_WP)
      coeff(3) = A - 2*B - 3*B*B
      coeff(2) = -(1-B)
      coeff(1) = 1

      ! Solve the cubic
      root = CubicRoot(coeff)
      root = root*(8.314_WP)*T/p

      ! Choose the liquid density
      rho_g_PR(i) = fuel%MW(i)/max(root(1),root(2),root(3))
   end do
      
 end function rho_g_PR

 function rho_l_PR(p,T)
   real(WP), intent(in)::p
   real(WP), intent(in)::T
   real(WP), dimension(fuelMat%numRows)::rho_l_PR

   real(WP), dimension(fuelMat%numRows)::alpha

   real(WP)::A
   real(WP)::B
   real(WP)::coeff(4)
   real(WP)::root(3)

   integer::i

   alpha = (1.0_WP + (fuel%kappa)*(1.0_WP - ((T/fuel%Tc)**0.5_WP) ))**2.0_WP

   do i = 1, fuelMat%numRows
      A = fuel%a(i)*alpha(i)*p/((8.314_WP**2.0_WP)*(T**2.0_WP))
      B = fuel%b(i)*p/((8.314_WP)*T)

      ! Set coefficients for Peng-Robinson
      coeff(4) = -(A*B - B**2.0_WP - B**3.0_WP)
      coeff(3) = A - 2*B - 3*B*B
      coeff(2) = -(1-B)
      coeff(1) = 1

      ! Solve the cubic
      root =  CubicRoot(coeff)
      root = root*(8.314_WP)*T/p

      ! Choose the liquid density
      rho_l_PR(i) = fuel%MW(i)/min(root(1),root(2),root(3))
   end do
      
 end function rho_l_PR

 function Hv(T)
   real(WP), intent(in)::T
   real(WP), dimension(fuelMat%numRows)::Hv
   integer::i

   Hv = fuel%Hv/fuel%MW

   ! THODOS CORRECTION
   do i = 1,fuelMat%numRows
      if (T > fuel%Tc(i)) then
         Hv(i) = 0
      else
         Hv(i) = Hv(i)*((1.0_WP - (T/fuel%Tc(i)))/(1.0_WP - (298/fuel%Tc(i))))**(0.38_WP)
      end if
   end do
 end function Hv

 function c_l(T)
   real(WP), intent(in)::T
   real(WP)::theta
   real(WP), dimension(fuelMat%numRows)::c_l

   theta = (T - 298.0_WP)/700.0_WP
   
   c_l = (fuel%CpA + (fuel%CpB)*theta + &
         (fuel%CpC)*(theta**(2.0_WP)))/(fuel%MW)
 end function c_l
 
 function mu_g(p,T)
   real(WP), intent(in)::p
   real(WP), intent(in)::T

   real(WP), dimension(fuelMat%numRows)::xi_T
   real(WP), dimension(fuelMat%numRows)::rho_r

   real(WP), dimension(fuelMat%numRows)::mu_g
   
   mu_g = 16.64_WP*((1E3_WP*fuel%MW)**0.5_WP)*T / &
          ((fuel%eps**0.5_WP)*((1E10_WP*fuel%sigma)**2.0_WP))

   rho_r = rho_g_PR(p,T)/(fuel%MW/fuel%Vc)
   xi_T = fuel%Tc/((((1E3_WP*fuel%MW)**3.0_WP)*((fuel%Pc/101325)**4.0_WP))**(1.0_WP/6.0_WP))

   mu_g = (1E-7_WP)*Jossi(mu_g,rho_r,xi_T)

 end function mu_g
 
 function mu_l(p,T)
   real(WP), intent(in)::p
   real(WP), intent(in)::T

   real(WP), dimension(fuelMat%numRows)::xi_T
   real(WP), dimension(fuelMat%numRows)::rho_r

   real(WP), dimension(fuelMat%numRows)::mu_l
   
   mu_l = 16.64_WP*((1E3_WP*fuel%MW)**0.5_WP)*T / &
          ((fuel%eps**0.5_WP)*((1E10_WP*fuel%sigma)**2.0_WP))

   rho_r = rho_l_PR(p,T)/(fuel%MW/fuel%Vc)
   xi_T= fuel%Tc/((((1E3_WP*fuel%MW)**3.0_WP)*((fuel%Pc/101325)**4.0_WP))**(1.0_WP/6.0_WP))
      
   mu_l = (1E-7_WP)*Jossi(mu_l,rho_r,xi_T)

 end function mu_l

 function omegaD(T)
   real(WP), dimension(fuelMat%numRows), intent(in)::T
   real(WP), dimension(fuelMat%numRows)::omegaD

   omegaD = (1.06036_WP)/(T**(0.15610_WP)) + (0.193_WP)/exp(0.47635*T) + &
            (1.03587_WP)/exp(1.52996*T) + (1.76474_WP)/exp(3.89411*T)

 end function omegaD
   
 function D(p,T)
   ! TODO : Implement high pressure gas diffusion routine
   real(WP), intent(in)::p
   real(WP), intent(in)::T

   real(WP), dimension(fuelMat%numRows)::Tstar
   real(WP), dimension(fuelMat%numRows)::MWa
   real(WP), dimension(fuelMat%numRows)::D

   real(WP), parameter::MW_air = 28.966_WP
   real(WP), parameter::Sigma_air = (1E-10_WP)*3.711
   real(WP), parameter::eps_air = 78.6

   MWa = (1E3_WP)*(2*fuel%MW*MW_air/(fuel%MW + MW_air))
   Tstar = T/((fuel%eps*eps_air)**0.5_WP)

   D = (1E-27_WP)*(3.03_WP - 0.98_WP/(MWa**0.5_WP))*(T**1.5_WP)/ &
       ((p/101325.0_WP)*(MWa**0.5_WP)*(0.25_WP*(fuel%Sigma + Sigma_air) &
       **2.0_WP)*omegaD(Tstar))

 end function D
 
 function f0(Tr)
   real(WP), dimension(fuelMat%numRows), intent(in)::Tr
   real(WP), dimension(fuelMat%numRows)::f0

   f0 = 5.92714_WP - 6.09648_WP/Tr - (1.28862_WP)*log(Tr) + &
        0.169347_WP*(Tr**6.0_WP)
 end function f0

 function f1(Tr)
   real(WP), dimension(fuelMat%numRows), intent(in)::Tr
   real(WP), dimension(fuelMat%numRows)::f1

   f1 = 15.2518_WP - 15.6875_WP/Tr - 13.4721_WP*log(Tr) + 0.43577_WP*(Tr**6.0_WP)
 end function f1

 function PSat(T)
   real(WP), intent(in)::T

   real(WP), dimension(fuelMat%numRows)::PSat

   PSat = (fuel%Pc)*exp(f0(T/fuel%Tc) + (fuel%omega)*f1(T/fuel%Tc))
 end function PSat

 function surf(T)
   real(WP), intent(in)::T

   real(WP), dimension(fuelMat%numRows)::surf

   do i = 1, fuelMat%numRows
      if (T > fuel%Tc(i)) then
         surf(i) = 0
      else
         surf(i) = (1E-3_WP/19.05_WP)*((fuel%Pc(i)/101325.0_WP)**(2.0_WP/3.0_WP)) &
                   *(fuel%Tc(i)**(1.0_WP/3.0_WP)) & 
                   *(1.86_WP + 1.18_WP*fuel%omega(i)) &
                   *((3.75_WP + 0.91*(fuel%omega(i)))/(0.291_WP - 0.08*fuel%omega(i))) &
                   **(2.0_WP/3.0_WP)*(1 - T/fuel%Tc(i))**(11.0_WP/9.0_WP)
      end if
   end do
 end function surf

 function activityTwoArgs(xVec,T)
	 real(WP), intent(in)::T
	 real(WP), dimension(fuelMat%numRows), intent(in)::xVec
	 
	 real(WP), dimension(fuelMat%numRows)::activityTwoArgs
	 
	 
	 real(WP), parameter::z = 10_WP
	 
	 ! Additional variables for routine
	 real(WP), dimension(fuelMat%numRows)::LVec
	 real(WP), dimension(fuelMat%numRows)::ThetaVec
	 real(WP), dimension(fuelMat%numRows)::PhiVec
	 real(WP), dimension(fuelMat%numRows)::gammaCVec
	 real(WP), dimension(fuelMat%numRows)::gammaRVec
	 real(WP), dimension(fuelMat%numRows)::GammaIVec
	 
	 real(WP), dimension(fuelMat%numColumns)::capXVec
	 real(WP), dimension(fuelMat%numColumns)::capXIVec
	 real(WP), dimension(fuelMat%numColumns)::capThetaVec
	 real(WP), dimension(fuelMat%numColumns)::capThetaIVec
	 real(WP), dimension(fuelMat%numColumns)::divRes
	 real(WP), dimension(fuelMat%numColumns)::GammaVec
	 real(WP), dimension(fuelMat%numColumns)::ThetaIVec
	 real(WP), dimension(fuelMat%numColumns,fuelMat%numColumns)::psi
	 
	 ! Energy interaction parameter
	 psi = exp(-fuel%aMat/T)
	 
	 LVec = (z/2.0_WP)*(fuel%R - fuel%Q) - (fuel%R - 1.0_WP)
	 
	 thetaVec = xVec*fuel%Q
	 thetaVec = thetaVec/sum(thetaVec)
	 
	 phiVec = xVec*fuel%R
	 phiVec = phiVec/sum(phiVec)
	 
	 gammaCVec = exp(log(phiVec/xVec) + (z/2.0_WP)*fuel%Q*log(thetaVec/phiVec) + &
	 			 LVec - (phiVec/xVec)*sum(xVec*LVec))
				 
	 call setnan(gammaCVec,size(gammaCVec),1.0_WP)
	 
	 ! Residual Activity
	 capXVec = matmul(xVec,fuelMat%groupCounts)
	 capXVec = capXVec/sum(capXVec)
	 
	 capThetaVec = fuel%capQ*capXVec
	 capThetaVec = capThetaVec/sum(capThetaVec)
	 
	 divRes = capThetaVec/matmul(capThetaVec,psi)
	 call setnan(divRes,size(divRes),0.0_WP)
	 
	 GammaVec = fuel%capQ*(1.0_WP - log(matmul(capThetaVec,psi)) - matmul(divRes,transpose(psi)) )
	 
	 do i = 1, fuelMat%numRows
		 capXIVec = fuelMat%groupCounts(i,:)/(sum(fuelMat%groupCounts(i,:)) + 0.0)
		 
		 capThetaIVec = fuel%capQ*capXIVec
		 capThetaIVec = capThetaIVec/sum(capThetaIVec)
		 
		 divRes = capThetaIVec/matmul(capThetaIVec,psi)
		 call setnan(divRes,size(divRes),0.0_WP)
		 
		 GammaIVec = fuel%capQ*(1.0_WP - log(matmul(capThetaIVec,psi)) - matmul(divRes,transpose(psi)) )
		 
		 GammaRVec(i) = exp(dot_product(fuelMat%groupCounts(i,:), GammaVec - GammaIVec))
	 end do	
	 
	 activityTwoArgs = gammaCVec*gammaRVec
 end function activityTwoArgs
 
 function activityOneArg(T)
	 real(WP), intent(in)::T
	 real(WP), dimension(fuelMat%numRows)::activityOneArg
	 
	 activityOneArg = activityTwoArgs(fuelMat%x_init,T)
 end function activityOneArg

end module dep_properties
