module compound_properties
  use precision
  use readFuel
  use readProp
  implicit none

contains

  function MWVec()
    real(WP), dimension(fuelMat%numRows)::MWVec
    real(WP), dimension(fuelMat%numColumns)::groupMW
    integer::i, index

    do i=1,fuelMat%numColumns
       index = fuelMat%indexList(i)
       groupMW(i) = propMat%MW(index)
    end do

    MWVec = (1E-3_WP)*matmul(fuelMat%groupCounts,groupMW)
    return
  end function MWVec

  function VcVec()
    real(WP), dimension(fuelMat%numRows)::VcVec
    real(WP), dimension(fuelMat%numColumns)::VcCo
    integer::i, index

    do i=1,fuelMat%numColumns
       index = fuelMat%indexList(i)
       VcCo(i) = propMat%VC(index)
    end do

    VcVec = (1E-3_WP)*(-0.00435_WP + matmul(fuelMat%groupCounts,VcCo))
    return
  end function VcVec

  function TcVec()
    real(WP), dimension(fuelMat%numRows)::TcVec
    real(WP), dimension(fuelMat%numColumns)::TcCo
    integer::i, index

    do i=1,fuelMat%numColumns
       index = fuelMat%indexList(i)
       TcCo(i) = propMat%TC(index)
    end do

    TcVec = (181.128_WP)*log(matmul(fuelMat%groupCounts,TcCo))
    return
  end function TcVec

  function PcVec()
    real(WP), dimension(fuelMat%numRows)::PcVec
    real(WP), dimension(fuelMat%numColumns)::PcCo
    integer::i, index

    do i=1,fuelMat%numColumns
       index = fuelMat%indexList(i)
       PcCo(i) = propMat%PC(index)
    end do

    PcVec = (101325_WP)*(1.3705_WP + (0.10022_WP + & 
             matmul(fuelMat%groupCounts,PcCo))**(-2.0_WP))
    return
  end function PcVec

  function HvVec()
    real(WP), dimension(fuelMat%numRows)::HvVec
    real(WP), dimension(fuelMat%numColumns)::HvCo
    integer::i, index

    do i=1,fuelMat%numColumns
       index = fuelMat%indexList(i)
       HvCo(i) = propMat%HV(index)
    end do

    HvVec = (1E3_WP)*((6.829_WP) + (matmul(fuelMat%groupCounts,HvCo)))
    return
  end function HvVec

  function CpA()
    real(WP), dimension(fuelMat%numRows)::CpA
    real(WP), dimension(fuelMat%numColumns)::CpACo
    integer::i, index

    do i=1,fuelMat%numColumns
       index = fuelMat%indexList(i)
       CpACo(i) = propMat%CPA(index)
    end do

    CpA = matmul(fuelMat%groupCounts,CpACo) - 19.7779_WP

    return
  end function CpA

  function CpB()
    real(WP), dimension(fuelMat%numRows)::CpB
    real(WP), dimension(fuelMat%numColumns)::CpBCo
    integer::i, index

    do i=1,fuelMat%numColumns
       index = fuelMat%indexList(i)
       CpBCo(i) = propMat%CPB(index)
    end do

    CpB = matmul(fuelMat%groupCounts,CpBCo) + 22.5981_WP

    return
  end function CpB

  function CpC()
    real(WP), dimension(fuelMat%numRows)::CpC
    real(WP), dimension(fuelMat%numColumns)::CpCCo
    integer::i, index

    do i=1,fuelMat%numColumns
       index = fuelMat%indexList(i)
       CpCCo(i) = propMat%CPC(index)
    end do

    CpC = matmul(fuelMat%groupCounts,CpCCo) - 10.7983_WP

    return
  end function CpC

  function omegaVec()
    real(WP), dimension(fuelMat%numRows)::omegaVec
    real(WP), dimension(fuelMat%numColumns)::omegaCo
    integer::i, index

    do i=1,fuelMat%numColumns
       index = fuelMat%indexList(i)
       omegaCo(i) = propMat%OMEGA(index)
    end do

    omegaVec = (0.4085_WP)*(log(matmul(fuelMat%groupCounts,omegaCo) + & 
             1.1507_WP))**(1.0_WP/0.5050_WP)
    return
  end function omegaVec

  function specVol_lVec()
    real(WP), dimension(fuelMat%numRows)::specVol_lVec
    real(WP), dimension(fuelMat%numColumns)::VliqCo
    integer::i, index

    do i=1,fuelMat%numColumns
      index = fuelMat%indexList(i)
      VliqCo(i) = propMat%VLIQ(index)
   end do
    specVol_lVec = (1E-3_WP)*(-0.00435_WP + &
              (matmul(fuelMat%groupCounts,VliqCo)) )
    return
  end function specVol_lVec

  function epsVec()
    real(WP), dimension(fuelMat%numRows)::epsVec
    
    epsVec = (0.7915_WP + (0.1693_WP)*omegaVec())*(TcVec())
  end function epsVec

  function sigmaVec()
    real(WP), dimension(fuelMat%numRows)::sigmaVec

    sigmaVec = (1E-10_WP)*(2.3551_WP - (0.0874_WP)*omegaVec())* &
               ((101325_WP)*TcVec()/PcVec())**(1.0_WP/3.0_WP)
  end function sigmaVec

  function aVec()
    real(WP), dimension(fuelMat%numRows)::aVec

    aVec = (0.457235_WP)*(8.314_WP**2.0_WP)*(TcVec()**2.0_WP)/PcVec()
  end function aVec

  function bVec()
    real(WP), dimension(fuelMat%numRows)::bVec

    bVec = (0.077796_WP)*(8.314_WP)*TcVec()/PcVec()
  end function bVec

  function kappaVec()
    real(WP), dimension(fuelMat%numRows)::kappaVec

    kappaVec = 0.37464_WP + 1.54226_WP*omegaVec() &
               -0.26992_WP*(omegaVec()**2.0_WP)
  end function kappaVec

  function ZcVec()
    real(WP), dimension(fuelMat%numRows)::ZcVec

    ZcVec = 0.29056_WP - 0.08775*omegaVec()
  end function ZcVec
  
  !!!!!!!!!!!!!!!!!!!!!!!
  ! Activity coefficient
  !!!!!!!!!!!!!!!!!!!!!!!
  
  function QVec()
    real(WP), dimension(fuelMat%numRows)::QVec
    real(WP), dimension(fuelMat%numColumns)::groupQ
    integer::i, index

    do i=1,fuelMat%numColumns
       index = fuelMat%indexList(i)
       groupQ(i) = propMat%Q(index)
    end do

    QVec = matmul(fuelMat%groupCounts,groupQ)
    return
  end function QVec
  
  function RVec()
    real(WP), dimension(fuelMat%numRows)::RVec
    real(WP), dimension(fuelMat%numColumns)::groupR
    integer::i, index

    do i=1,fuelMat%numColumns
       index = fuelMat%indexList(i)
       groupR(i) = propMat%R(index)
    end do

    RVec = matmul(fuelMat%groupCounts,groupR)
    return
  end function RVec
  
  function capQVec()
	  real(WP), dimension(fuelMat%numColumns)::capQVec
	  integer::i, index
	  
      do i=1,fuelMat%numColumns
         index = fuelMat%indexList(i)
         capQVec(i) = propMat%Q(index)
      end do
	  return
  end function capQVec
  
  function capRVec()
	  real(WP), dimension(fuelMat%numColumns)::capRVec
	  integer::i, index
	  
      do i=1,fuelMat%numColumns
         index = fuelMat%indexList(i)
         capRVec(i) = propMat%R(index)
      end do
	  return
  end function capRVec
  
  function aMat()
	  real(WP), dimension(fuelMat%numColumns, fuelMat%numColumns)::aMat
	  integer::i, j, indexA, indexB
	  
      do i=1,fuelMat%numColumns
		  do j=1, fuelMat%numColumns 
         	 indexA = fuelMat%indexList(i)
			 indexB = fuelMat%indexList(j)
         	 aMat(i,j) = propMat%A(indexA, indexB)
      	  end do
	  end do
	  return
  end function aMat
	  
  
end module compound_properties
