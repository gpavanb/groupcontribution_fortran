clc; clear all; close all;
[num,txt] = xlsread('isocetane_old.xlsx');
toDel = [];
indexList = 1:size(num,2);
for i = 1:size(num,2)
    if (nnz(num(:,i)) == 0)
        toDel = [toDel i];
    end
end
num(:,toDel) = [];
indexList(:,toDel) = [];

dlmwrite('isocetane_old.txt',[size(num,1),size(num,2)]);
dlmwrite('isocetane_old.txt',indexList,'-append');
dlmwrite('isocetane_old.txt',num,'-append');