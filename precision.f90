! REPLACE THIS WITH precision.f90 IN 3DA
module precision
  implicit none
  integer, parameter, private :: SP = kind(1.0)
  integer, parameter, private :: DP = kind(1.0d0)
  integer, parameter :: WP = DP
end module precision
