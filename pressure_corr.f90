module pressure_corr
  use fuelProp, only : fuel
  use precision
  use readFuel, only : fuelMat
  implicit none

contains

  function Lucas(Tr,xi)
    real(WP), dimension(fuelMat%numRows), intent(in)::Tr
    real(WP), dimension(fuelMat%numRows), intent(in)::xi
    real(WP), dimension(fuelMat%numRows)::Lucas
    
    Lucas = ((0.807_WP)*(Tr**(0.618_WP)) - (0.357_WP)*exp((-0.449_WP)*Tr) + &
            (0.340_WP)*exp((-4.058_WP)*Tr) + (0.018_WP))/xi
  end function Lucas

  function Jossi(mu0, rho_r, xi_T)
    real(WP), dimension(fuelMat%numRows), intent(in)::mu0
    real(WP), dimension(fuelMat%numRows), intent(in)::rho_r
    real(WP), dimension(fuelMat%numRows), intent(in)::xi_T
    real(WP), dimension(fuelMat%numRows)::Jossi

    Jossi = ((1.0230_WP + 0.23364_WP*rho_r + 0.58533_WP*(rho_r**(2.0_WP)) - &
            0.40758_WP*(rho_r**3.0_WP) + 0.0933324_WP*(rho_r**4.0_WP))** &
            (4.0_WP) - 1.0_WP)/xi_T + mu0
  end function Jossi

end module pressure_corr
