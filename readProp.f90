module readProp
  use limits
  use precision
  implicit none

  type propMat_type
    real(WP), dimension(NG)::TC
    real(WP), dimension(NG)::PC
    real(WP), dimension(NG)::VC
    real(WP), dimension(NG)::OMEGA
    real(WP), dimension(NG)::HV
    real(WP), dimension(NG)::VLIQ
    real(WP), dimension(NG)::CPA
    real(WP), dimension(NG)::CPB
    real(WP), dimension(NG)::CPC
    real(WP), dimension(NG)::MW
	
	! Activity calculation input
	real(WP), dimension(NG)::Q
	real(WP), dimension(NG)::R
    real(WP), dimension(NG,NG)::A
  end type propMat_type

  type(propMat_type)::propMat
  save

contains
  ! use parser and replace read statements
  subroutine readPropTable
    integer::allocateStatus
    integer::IOStatus
    
    open(1,file="data/solver_input/gani_prop_table.csv",& 
         status='old',action='read',iostat=IOStatus)

    if (IOStatus > 0) then
       print *, "ERROR: Unable to open input properties file"
       stop
    end if

    read(1,*) propMat%TC, & 
    propMat%PC, &
    propMat%VC, &
    propMat%OMEGA, &
    propMat%HV, &
    propMat%VLIQ, &
    propMat%CPA, &
    propMat%CPB, &
    propMat%CPC, &
    propMat%MW, &
	propMat%R, &
	propMat%Q

    close (1)
  end subroutine readPropTable

  ! Use parser and replace read statements
  subroutine readActTable
    integer::allocateStatus
    integer::IOStatus
    
    open(1,file="data/solver_input/a.csv",& 
         status='old',action='read',iostat=IOStatus)

    if (IOStatus > 0) then
       print *, "ERROR: Unable to open activity coefficients file"
       stop
    end if

    read(1,*) propMat%A
    
    close (1)
  end subroutine readActTable
end module readProp
