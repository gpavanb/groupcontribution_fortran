test_suite fuelProp

setup
  call readPropTable
  call readFuelTable("posf10325")
  call setFuelProp
end setup

teardown
  call readFuel_deallocate
  call fuelProp_deallocate
end teardown

! Test all values for the first, middle and last
test MW_test
  ! Molecular weight
  assert_equal_within(fuel%MW(1),0.0920,1e-5)
  assert_equal_within(fuel%MW(33),0.2820,1e-5)
  assert_equal_within(fuel%MW(fuelMat%numRows),0.1650,1e-5)
end test

test Vc_test
  ! Critical Volume
  assert_equal_within(fuel%Vc(1),3.1025e-4,1e-8)
  assert_equal_within(fuel%Vc(33),1.14495e-3,1e-8)
  assert_equal_within(fuel%Vc(fuelMat%numRows),5.9235e-4,1e-8)
end test

test Tc_test
  ! Critical Temperature
  assert_equal_within(fuel%Tc(1),596.1716,1e-4)
  assert_equal_within(fuel%Tc(33),755.9292,1e-4)
  assert_equal_within(fuel%Tc(fuelMat%numRows),683.4650,1e-4)
end test

test Pc_test
  ! Critical Pressure
  assert_equal_within(fuel%Pc(1),4.2433e6,10)
  assert_equal_within(fuel%Pc(33),1.0647e6,10)
  assert_equal_within(fuel%Pc(fuelMat%numRows),2.6842e6,10)
end test

test CpA_test
  ! Specific heat
  assert_equal_within(fuel%CpA(1),104.976,1e-4)
  assert_equal_within(fuel%CpA(34),163.6255,1e-4)
  assert_equal_within(fuel%CpA(fuelMat%numRows),210.7151,1e-4)
end test

test CpB_test
  ! Specific heat
  assert_equal_within(fuel%CpB(1),251.961,1e-4)
  assert_equal_within(fuel%CpB(34),327.2492,1e-4)
  assert_equal_within(fuel%CpB(fuelMat%numRows),608.3736,1e-4)
end test

test CpC_test
  ! Specific heat
  assert_equal_within(fuel%CpC(1),-97.7113,1e-4)
  assert_equal_within(fuel%CpC(34),-109.1612,1e-4)
  assert_equal_within(fuel%CpC(fuelMat%numRows),-240.6709,1e-4)
end test

test Hv_test
  ! Latent Heat
  assert_equal_within(fuel%Hv(1),37.095e3,1e-2)
  assert_equal_within(fuel%Hv(33),96.348e3,1e-2)
  assert_equal_within(fuel%Hv(fuelMat%numRows),56.992e3,1e-2)
end test

test omega_test
  ! Acentric Factor
  assert_equal_within(fuel%omega(1),53.68607e-3,1e-5)
  assert_equal_within(fuel%omega(33),867.9962e-3,1e-5)
  assert_equal_within(fuel%omega(fuelMat%numRows),272.8839e-3,1e-5)
end test

test specVol_test
  assert_equal_within(fuel%specVol(1),90.55e-6,1e-10)
  assert_equal_within(fuel%specVol(33),343.45e-6,1e-10)
  assert_equal_within(fuel%specVol(fuelMat%numRows),164.55e-6,1e-10)
end test

test eps_test
  ! Lennard-Jones energy
  assert_equal_within(fuel%eps(1),477.288487,1e-4)
  assert_equal_within(fuel%eps(33),709.40308,1e-4)
  assert_equal_within(fuel%eps(fuelMat%numRows),572.538145,1e-4)
end test

test sigma_test
  ! Lennard-Jones Radius
  assert_equal_within(fuel%sigma(1),569.645e-12,1e-15)
  assert_equal_within(fuel%sigma(33),947.937e-12,1e-15)
  assert_equal_within(fuel%sigma(fuelMat%numRows),688.856e-12,1e-15)
end test

! TODO : Test new properties for Peng-Robinson

end test_suite

