test_suite cubic

test Single_Real
  real(WP), dimension(4)::coeff
  real(WP), dimension(3)::root

  coeff(1) = 1.0_WP
  coeff(2) = -3.0_WP
  coeff(3) = 3.0_WP 
  coeff(4) = -1.0_WP

  root = CubicRoot(coeff) 
   
  assert_equal_within(root(1),1.0,1e-4)
  assert_equal_within(root(2),1.0,1e-4)
  assert_equal_within(root(3),1.0,1e-4)
end test

test Three_Real
  real(WP), dimension(4)::coeff
  real(WP), dimension(3)::root

  coeff(1) = 1.0_WP
  coeff(2) = -6.0_WP
  coeff(3) = 11.0_WP
  coeff(4) = -6.0_WP

  root = CubicRoot(coeff)

  assert_equal_within(root(1),1.0,1e-4)
  assert_equal_within(root(2),3.0,1e-4)
  assert_equal_within(root(3),2.0,1e-4)
end test

end test_suite