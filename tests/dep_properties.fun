test_suite dep_properties

! Remove only keywords to run this test
setup
  call readPropTable
  call readFuelTable("posf10325")
  call setFuelProp
end setup

teardown
  call readFuel_deallocate
  call fuelProp_deallocate
end teardown

test surf_test
  real(WP), dimension(fuelMat%numRows)::surfVec

  ! TODO : Seems like low accuracy

  surfVec = surf(300.0_WP)
  assert_equal_within(surfVec(1),24.18803e-3,5e-4)
  assert_equal_within(surfVec(33),26.46679e-3,5e-4)
  assert_equal_within(surfVec(fuelMat%numRows), 29.76731e-3,5e-4)

  surfVec = surf(500.0_WP)
  assert_equal_within(surfVec(1),6.117153e-3,5e-4)
  assert_equal_within(surfVec(33),13.026568e-3,5e-4)
  assert_equal_within(surfVec(fuelMat%numRows),11.348026e-3,5e-4)

  surfVec = surf(700.0_WP)
  assert_equal_within(surfVec(1),0,5e-4)
  assert_equal_within(surfVec(33),1.978389e-3,5e-4)
  assert_equal_within(surfVec(fuelMat%numRows),0,5e-4)
end test

test rho_l_test
  ! Low accuracy due to specific volume accuracy
  real(WP), dimension(fuelMat%numRows)::rho

  rho = rho_l(300.0_WP)
  assert_equal_within(rho(1),1.012010e3,10)
  assert_equal_within(rho(33),814.562349,10)
  assert_equal_within(rho(fuelMat%numRows), 998.72006,10)

  rho = rho_l(600.0_WP)
  assert_equal_within(rho(1),363.67414,10)
  assert_equal_within(rho(33),576.55186,10)
  assert_equal_within(rho(fuelMat%numRows),636.95963,10)

  rho = rho_l(900.0_WP)
  assert_equal_within(rho(1),363.67414,10)
  assert_equal_within(rho(33),215.09868,10)
  assert_equal_within(rho(fuelMat%numRows),310.33229,10)
end test

test c_l_test
  real(WP), dimension(fuelMat%numRows)::c

  c = c_l(300.0_WP)
  assert_equal_within(c(1),1.14885e3,1)
  assert_equal_within(c(33),1.63071e3,1)
  assert_equal_within(c(fuelMat%numRows),1.34473e3,1)

  c = c_l(600.0_WP)
  assert_equal_within(c(1),2.124914e3,1)
  assert_equal_within(c(33),2.819272e3,1)
  assert_equal_within(c(fuelMat%numRows),2.59168e3,1)

  ! Extrapolation Regime - Test pass but deviates from experimental
  c = c_l(900.0_WP)
  assert_equal_within(c(1),2.710817e3,1)
  assert_equal_within(c(33),3.589165e3,1)
  assert_equal_within(c(fuelMat%numRows),3.335993e3,1)
end test

test Hv_test
  ! Supercritical Latent heat is zero
  real(WP), dimension(fuelMat%numRows)::H

  H = Hv(300.0_WP)
  assert_equal_within(H(1),403.2065e3,1e-1)
  assert_equal_within(H(33),342.6950e3,1e-1)
  assert_equal_within(H(fuelMat%numRows),345.4060e3,1e-1)

  H = Hv(600.0_WP)
  assert_equal_within(H(1),0,1e-5)
  assert_equal_within(H(33),227.021716e3,1e-1)
  assert_equal_within(H(fuelMat%numRows),180.194212e3,1e-1)

  H = Hv(900.0_WP)
  assert_equal_within(H(1),0,1e-5)
  assert_equal_within(H(33),0,1e-5)
  assert_equal_within(H(fuelMat%numRows),0,1e-5)
end test

test mu_g_test
  real(WP), dimension(fuelMat%numRows)::mu_gVec

  mu_gVec = mu_g(101325.0_WP,300.0_WP)
  assert_equal_within(mu_gVec(1),4.33278079316881e-006,1e-10)
  assert_equal_within(mu_gVec(33),1.17312523574307e-006,1e-10)
  assert_equal_within(mu_gVec(fuelMat%numRows),2.47269434354950e-006,1e-10)

  mu_gVec = mu_g(1e6_WP,300.0_WP)
  assert_equal_within(mu_gVec(1),439.019013867830e-009,1e-13)
  assert_equal_within(mu_gVec(33),118.866914511666e-009,1e-13)
  assert_equal_within(mu_gVec(fuelMat%numRows),250.545754360153e-009,1e-13)

  mu_gVec = mu_g(5e6_WP,300.0_WP)
  assert_equal_within(mu_gVec(1),87.8038027735660e-009,1e-13)
  assert_equal_within(mu_gVec(33),23.7733829023333e-009,1e-13)
  assert_equal_within(mu_gVec(fuelMat%numRows),50.1091508720307e-009,1e-13)

  mu_gVec = mu_g(101325.0_WP,600.0_WP)
  assert_equal_within(mu_gVec(1),15.4000759430440e-006,1e-10)
  assert_equal_within(mu_gVec(33),4.29166298579623e-006,1e-10)
  assert_equal_within(mu_gVec(fuelMat%numRows),8.90446088636636e-006,1e-10)

  mu_gVec = mu_g(1e6_WP,600.0_WP)
  assert_equal_within(mu_gVec(1),1.56041269492894e-006,1e-10)
  assert_equal_within(mu_gVec(33),434.852752035803e-009,1e-13)
  assert_equal_within(mu_gVec(fuelMat%numRows),902.244499311072e-009,1e-13)

  mu_gVec = mu_g(5e6_WP,600.0_WP)
  assert_equal_within(mu_gVec(1),312.082538985788e-009,1e-13)
  assert_equal_within(mu_gVec(33),86.9705504071606e-009,1e-13)
  assert_equal_within(mu_gVec(fuelMat%numRows),180.448899862214e-009,1e-13)

  mu_gVec = mu_g(101325.0_WP,900.0_WP)
  assert_equal_within(mu_gVec(1),31.1254047802873e-006,1e-10)
  assert_equal_within(mu_gVec(33),8.77077087142076e-006,1e-10)
  assert_equal_within(mu_gVec(fuelMat%numRows),18.0820321011104e-006,1e-10)

  mu_gVec = mu_g(1e6_WP,900.0_WP)
  assert_equal_within(mu_gVec(1),3.15378163936261e-006,1e-10)
  assert_equal_within(mu_gVec(33),888.698358546708e-009,1e-13)
  assert_equal_within(mu_gVec(fuelMat%numRows),1.83216190264501e-006,1e-10)

  mu_gVec = mu_g(5e6_WP,900.0_WP)
  assert_equal_within(mu_gVec(1),630.756327872522e-009,1e-13)
  assert_equal_within(mu_gVec(33),177.739671709342e-009,1e-13)
  assert_equal_within(mu_gVec(fuelMat%numRows),366.432380529003e-009,1e-13)
  
end test

test mu_l_test
  real(WP), dimension(fuelMat%numRows)::mu_lVec

  mu_lVec = mu_l(101325.0_WP,300.0_WP)
  assert_equal_within(mu_lVec(1),4.33278079316881e-006,1e-10)
  assert_equal_within(mu_lVec(33),1.17312523574307e-006,1e-10)
  assert_equal_within(mu_lVec(fuelMat%numRows),2.47269434354950e-006,1e-10)

  mu_lVec = mu_l(1e6_WP,300.0_WP)
  assert_equal_within(mu_lVec(1),439.019013867830e-009,1e-13)
  assert_equal_within(mu_lVec(33),118.866914511666e-009,1e-13)
  assert_equal_within(mu_lVec(fuelMat%numRows),250.545754360153e-009,1e-13)

  mu_lVec = mu_l(5e6_WP,300.0_WP)
  assert_equal_within(mu_lVec(1),87.8038027735660e-009,1e-13)
  assert_equal_within(mu_lVec(33),23.7733829023333e-009,1e-13)
  assert_equal_within(mu_lVec(fuelMat%numRows),50.1091508720307e-009,1e-13)

  mu_lVec = mu_l(101325.0_WP,600.0_WP)
  assert_equal_within(mu_lVec(1),15.4000759430440e-006,1e-10)
  assert_equal_within(mu_lVec(33),4.29166298579623e-006,1e-10)
  assert_equal_within(mu_lVec(fuelMat%numRows),8.90446088636636e-006,1e-10)

  mu_lVec = mu_l(1e6_WP,600.0_WP)
  assert_equal_within(mu_lVec(1),1.56041269492894e-006,1e-10)
  assert_equal_within(mu_lVec(33),434.852752035803e-009,1e-13)
  assert_equal_within(mu_lVec(fuelMat%numRows),902.244499311072e-009,1e-13)

  mu_lVec = mu_l(5e6_WP,600.0_WP)
  assert_equal_within(mu_lVec(1),312.082538985788e-009,1e-13)
  assert_equal_within(mu_lVec(33),86.9705504071606e-009,1e-13)
  assert_equal_within(mu_lVec(fuelMat%numRows),180.448899862214e-009,1e-13)

  mu_lVec = mu_l(101325.0_WP,900.0_WP)
  assert_equal_within(mu_lVec(1),31.1254047802873e-006,1e-10)
  assert_equal_within(mu_lVec(33),8.77077087142076e-006,1e-10)
  assert_equal_within(mu_lVec(fuelMat%numRows),18.0820321011104e-006,1e-10)

  mu_lVec = mu_l(1e6_WP,900.0_WP)
  assert_equal_within(mu_lVec(1),3.15378163936261e-006,1e-10)
  assert_equal_within(mu_lVec(33),888.698358546708e-009,1e-13)
  assert_equal_within(mu_lVec(fuelMat%numRows),1.83216190264501e-006,1e-10)

  mu_lVec = mu_l(5e6_WP,900.0_WP)
  assert_equal_within(mu_lVec(1),630.756327872522e-009,1e-13)
  assert_equal_within(mu_lVec(33),177.739671709342e-009,1e-13)
  assert_equal_within(mu_lVec(fuelMat%numRows),366.432380529003e-009,1e-13)
  
end test


test D_test
  real(WP), dimension(fuelMat%numRows)::diff

  diff = D(101325.0_WP,300.0_WP)
  assert_equal_within(diff(1),4.33278079316881e-006,1e-10)
  assert_equal_within(diff(33),1.17312523574307e-006,1e-10)
  assert_equal_within(diff(fuelMat%numRows),2.47269434354950e-006,1e-10)

  diff = D(1e6_WP,300.0_WP)
  assert_equal_within(diff(1),439.019013867830e-009,1e-13)
  assert_equal_within(diff(33),118.866914511666e-009,1e-13)
  assert_equal_within(diff(fuelMat%numRows),250.545754360153e-009,1e-13)

  diff = D(5e6_WP,300.0_WP)
  assert_equal_within(diff(1),87.8038027735660e-009,1e-13)
  assert_equal_within(diff(33),23.7733829023333e-009,1e-13)
  assert_equal_within(diff(fuelMat%numRows),50.1091508720307e-009,1e-13)

  diff = D(101325.0_WP,600.0_WP)
  assert_equal_within(diff(1),15.4000759430440e-006,1e-10)
  assert_equal_within(diff(33),4.29166298579623e-006,1e-10)
  assert_equal_within(diff(fuelMat%numRows),8.90446088636636e-006,1e-10)

  diff = D(1e6_WP,600.0_WP)
  assert_equal_within(diff(1),1.56041269492894e-006,1e-10)
  assert_equal_within(diff(33),434.852752035803e-009,1e-13)
  assert_equal_within(diff(fuelMat%numRows),902.244499311072e-009,1e-13)

  diff = D(5e6_WP,600.0_WP)
  assert_equal_within(diff(1),312.082538985788e-009,1e-13)
  assert_equal_within(diff(33),86.9705504071606e-009,1e-13)
  assert_equal_within(diff(fuelMat%numRows),180.448899862214e-009,1e-13)

  diff = D(101325.0_WP,900.0_WP)
  assert_equal_within(diff(1),31.1254047802873e-006,1e-10)
  assert_equal_within(diff(33),8.77077087142076e-006,1e-10)
  assert_equal_within(diff(fuelMat%numRows),18.0820321011104e-006,1e-10)

  diff = D(1e6_WP,900.0_WP)
  assert_equal_within(diff(1),3.15378163936261e-006,1e-10)
  assert_equal_within(diff(33),888.698358546708e-009,1e-13)
  assert_equal_within(diff(fuelMat%numRows),1.83216190264501e-006,1e-10)

  diff = D(5e6_WP,900.0_WP)
  assert_equal_within(diff(1),630.756327872522e-009,1e-13)
  assert_equal_within(diff(33),177.739671709342e-009,1e-13)
  assert_equal_within(diff(fuelMat%numRows),366.432380529003e-009,1e-13)
  
end test

test rho_g_PR_test
  real(WP), dimension(fuelMat%numRows)::rho

  rho = rho_g_PR(101325.0_WP,300.0_WP)
  assert_equal_within(rho(1),3.73743152738032e+000,1)
  assert_equal_within(rho(33),11.4560952137991e+000,1)
  assert_equal_within(rho(fuelMat%numRows),6.70300603617648e+000,1)

  rho = rho_g_PR(1e6_WP,300.0_WP)
  assert_equal_within(rho(1),36.8856415274749e+000,1)
  assert_equal_within(rho(33),113.067876680061e+000,1)
  assert_equal_within(rho(fuelMat%numRows),66.1539702685152e+000,1)

  rho = rho_g_PR(5e6_WP,300.0_WP)
  assert_equal_within(rho(1),184.429547357156e+000,1)
  assert_equal_within(rho(33),565.450848972170e+000,1)
  assert_equal_within(rho(fuelMat%numRows),330.779734643720e+000,1)

  rho = rho_g_PR(101325.0_WP,600.0_WP)
  assert_equal_within(rho(1),1.86871527567243e+000,1)
  assert_equal_within(rho(33),5.72801907399053e+000,1)
  assert_equal_within(rho(fuelMat%numRows),3.35150018570405e+000,1)

  rho = rho_g_PR(1e6_WP,600.0_WP)
  assert_equal_within(rho(1),18.4427732306251e+000,1)
  assert_equal_within(rho(33),56.5311590687294e+000,1)
  assert_equal_within(rho(fuelMat%numRows),33.0767092578176e+000,1)

  rho = rho_g_PR(5e6_WP,600.0_WP)
  assert_equal_within(rho(1),92.2135854253525e+000,1)
  assert_equal_within(rho(33),282.655929803096e+000,1)
  assert_equal_within(rho(fuelMat%numRows),165.382970716584e+000,1)

  rho = rho_g_PR(101325.0_WP,900.0_WP)
  assert_equal_within(rho(1),1.24581017440917e+000,1)
  assert_equal_within(rho(33),3.81867782470288e+000,1)
  assert_equal_within(rho(fuelMat%numRows),2.23433333019371e+000,1)

  rho = rho_g_PR(1e6_WP,900.0_WP)
  assert_equal_within(rho(1),12.2951812409125e+000,1)
  assert_equal_within(rho(33),37.6872876388788e+000,1)
  assert_equal_within(rho(fuelMat%numRows),22.0511271412288e+000,1)

  rho = rho_g_PR(5e6_WP,900.0_WP)
  assert_equal_within(rho(1),61.4757008021395e+000,1)
  assert_equal_within(rho(33),188.433493864817e+000,1)
  assert_equal_within(rho(fuelMat%numRows),110.255004758453e+000,1)
end test

test rho_l_PR_test
  real(WP), dimension(fuelMat%numRows)::rho

  rho = rho_l_PR(101325.0_WP,300.0_WP)
  assert_equal_within(rho(1),3.73743152738032e+000,1)
  assert_equal_within(rho(33),11.4560952137991e+000,1)
  assert_equal_within(rho(fuelMat%numRows),6.70300603617648e+000,1)

  rho = rho_l_PR(1e6_WP,300.0_WP)
  assert_equal_within(rho(1),36.8856415274749e+000,1)
  assert_equal_within(rho(33),113.067876680061e+000,1)
  assert_equal_within(rho(fuelMat%numRows),66.1539702685152e+000,1)

  rho = rho_l_PR(5e6_WP,300.0_WP)
  assert_equal_within(rho(1),184.429547357156e+000,1)
  assert_equal_within(rho(33),565.450848972170e+000,1)
  assert_equal_within(rho(fuelMat%numRows),330.779734643720e+000,1)

  rho = rho_l_PR(101325.0_WP,600.0_WP)
  assert_equal_within(rho(1),1.86871527567243e+000,1)
  assert_equal_within(rho(33),5.72801907399053e+000,1)
  assert_equal_within(rho(fuelMat%numRows),3.35150018570405e+000,1)

  rho = rho_l_PR(1e6_WP,600.0_WP)
  assert_equal_within(rho(1),18.4427732306251e+000,1)
  assert_equal_within(rho(33),56.5311590687294e+000,1)
  assert_equal_within(rho(fuelMat%numRows),33.0767092578176e+000,1)

  rho = rho_l_PR(5e6_WP,600.0_WP)
  assert_equal_within(rho(1),92.2135854253525e+000,1)
  assert_equal_within(rho(33),282.655929803096e+000,1)
  assert_equal_within(rho(fuelMat%numRows),165.382970716584e+000,1)

  rho = rho_l_PR(101325.0_WP,900.0_WP)
  assert_equal_within(rho(1),1.24581017440917e+000,1)
  assert_equal_within(rho(33),3.81867782470288e+000,1)
  assert_equal_within(rho(fuelMat%numRows),2.23433333019371e+000,1)

  rho = rho_l_PR(1e6_WP,900.0_WP)
  assert_equal_within(rho(1),12.2951812409125e+000,1)
  assert_equal_within(rho(33),37.6872876388788e+000,1)
  assert_equal_within(rho(fuelMat%numRows),22.0511271412288e+000,1)

  rho = rho_l_PR(5e6_WP,900.0_WP)
  assert_equal_within(rho(1),61.4757008021395e+000,1)
  assert_equal_within(rho(33),188.433493864817e+000,1)
  assert_equal_within(rho(fuelMat%numRows),110.255004758453e+000,1)
end test

test PSat_test
  real(WP), dimension(fuelMat%numRows)::Ps

  Ps = PSat(300.0_WP)
  assert_equal_within(Ps(1),14.612136e3,500)
  assert_equal_within(Ps(33),9.778191e-3,10)
  assert_equal_within(Ps(fuelMat%numRows),55.073818,10)

  Ps = PSat(600.0_WP)
  assert_equal_within(Ps(1),4.35415764218897e6,10)
  assert_equal_within(Ps(33),84.7049470791302e3,10)
  assert_equal_within(Ps(fuelMat%numRows),1.07689380577819e6,10)

  Ps = PSat(900.0_WP)
  assert_equal_within(Ps(1),153.644012940608e6,10)
  assert_equal_within(Ps(33),7.24502086683946e6,10)
  assert_equal_within(Ps(fuelMat%numRows),56.9726573470715e6,10)
end test

end test_suite