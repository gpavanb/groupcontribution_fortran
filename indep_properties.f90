module indep_properties
  use fuelProp, only : fuel
  use precision
  use readFuel, only : fuelMat
  implicit none

contains 
  function MW()
    real(WP), dimension(fuelMat%numRows)::MW

    MW = fuel%MW
    return
  end function MW

  function Vc()
    real(WP), dimension(fuelMat%numRows)::Vc

    Vc = fuel%Vc
    return
  end function Vc

  function Tc()
    real(WP), dimension(fuelMat%numRows)::Tc

    Tc = fuel%Tc
    return
  end function Tc

  function Pc()
    real(WP), dimension(fuelMat%numRows)::Pc

    Pc = fuel%Pc
    return
  end function Pc

  function omega()
    real(WP), dimension(fuelMat%numRows)::omega

    omega = fuel%omega
    return
  end function omega

  function specVol_l()
    real(WP), dimension(fuelMat%numRows)::specVol_l

    specVol_l = fuel%specVol
    return
  end function specVol_l

  function eps()
    real(WP), dimension(fuelMat%numRows)::eps

    eps = fuel%eps
    return
  end function eps

  function sigma()
    real(WP), dimension(fuelMat%numRows)::sigma

    sigma = fuel%sigma
    return
  end function sigma
end module indep_properties
