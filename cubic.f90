! ANALYTIC CUBIC ROOTS SOLUTION
module cubic
  use limits
  use precision

  implicit none

contains

 function CubicRoot(coeff)

 ! Finds ONLY REAL roots to coeff(1)*x**3 + coeff(2)*x**2 ... = 0

 real(WP), dimension(4), intent(in)::coeff

 real(WP), dimension(3)::CubicRoot

 real(WP), parameter::pi = 4*atan(1.0_WP)

 real(WP)::a, b, c
 real(WP)::Q, R
 real(WP)::theta
 real(WP)::AC, BC

 a = coeff(2)/coeff(1)
 b = coeff(3)/coeff(1)
 c = coeff(4)/coeff(1)

 Q = (a*a-3*b)/9
 R = (2*(a**3.0_WP)-(9*a*b)+27*c)/54

 ! THREE REAL ROOTS CASE
 if ((R**2.0_WP) < (Q**3.0_WP)) then
   theta = acos(R/sqrt(Q**3.0_WP))

   CubicRoot(1) = -2*sqrt(Q)*cos(theta/3)-a/3
   CubicRoot(2) = -2*sqrt(Q)*cos((theta + 2*pi)/3)-a/3
   CubicRoot(3) = -2*sqrt(Q)*cos((theta - 2*pi)/3)-a/3
   return
 else
 ! ONE REAL ROOT CASE
   AC = -sign(R,R)*((abs(R) + sqrt(R**2.0_WP - Q**3.0_WP))**(1.0_WP/3.0_WP))

   if (AC == 0) then
     BC = 0
   else
     BC = Q/AC
   end if

   CubicRoot(1) = (AC+BC)-a/3
   CubicRoot(2) = (AC+BC)-a/3
   CubicRoot(3) = (AC+BC)-a/3
   return
 end if
 end function
end module
